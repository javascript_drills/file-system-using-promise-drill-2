
const fs = require(`fs`);

const path = require(`path`);

const outputPath = "../output";

function retriveUsers() {

    return new Promise((resolve, reject) => {

        fs.readFile("../data.json", "utf8", (error, data) => {

            if (error) {
                reject(error);

            } else {

                const userData = JSON.parse(data);

                let arrayA = userData.employees;

                const ans = arrayA.filter(obj => obj.id === 2 || obj.id === 13 || obj.id === 23);

                fs.writeFile(path.join(outputPath, "retrivedUsers.json"), JSON.stringify(ans, null, 4), (error) => {

                    if (error) {
                        reject(error);
                    } else {

                        resolve(userData);

                    }

                });
            }

        });



    });

}


function groupCompanies(userData) {
   
  
    return new Promise((resolve, reject) => {

        const employees = userData.employees;


   

        const ans =employees.reduce((accumulator, obj) => {

            let comp = obj["company"];

            if (accumulator[comp] === undefined) {

                let temp = [];
                temp.push(obj);
                accumulator[comp] = temp;

            } else {

                accumulator[comp].push(obj);
            }

            return accumulator;
        }, {});

        fs.writeFile(path.join(outputPath, "groupCompanies.json"), JSON.stringify(ans, null, 4), (error) => {

            if (error) {

                reject(error);
            } else {
                resolve(userData);

            }

        });


    });

}

//     3. Get all data for company Powerpuff Brigade
function retriveCompany(data) {

    return new Promise((resolve, reject) => {

        const employees = data.employees;

        const ans = employees.filter(obj => obj.company === "Powerpuff Brigade");

        fs.writeFile(path.join(outputPath, "retriveCompany.json"), JSON.stringify(ans, null, 4), (error) => {

            if (error) {

                reject(error);
                return;
            } else {

                resolve(data);
            }

        });


    });

}


// 4. Remove entry with id 2.

function removeId(data, cb) {

    return new Promise((resolve, reject) => {


        const employees = data.employees;

        const ans = employees.filter(obj => obj.id !== 2);


        fs.writeFile(path.join(outputPath, "removeId.json"), JSON.stringify(ans, null, 4), (error) => {

            if (error) {

                reject(error);
                return;
            } else {

                resolve(data);
            }

        });

        resolve(data);

    });

}

// 5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.

function sortUsers(data) {

    return new Promise((resolve, reject) => {

        const employees = data.employees;

        const ans = employees.sort((a, b) => {

            if (a.company < b.company) {

                return -1;
            }
            else if (a.company === b.company) {

                if (a.id < b.id) {
                    return -1;
                }
            }


        });

        fs.writeFile(path.join(outputPath, "sortedUsers.json"), JSON.stringify(ans, null, 4), (error) => {

            if (error) {

                reject(error);
                return;
            } else {

                resolve(data);
            }

        });

        resolve(data);

    });

}

//6. Swap position of companies with id 93 and id 92.

function swapCompany(data, cb) {

    return new Promise((resolve, reject) => {

        const employees = data.employees;

        let c1 = employees.company;

        let id = employees;

        const emp1 = employees.filter(obj => obj.id === 93);

        const emp2 = employees.filter(obj => obj.id === 92);

        let a = emp2[0]["company"];

        let b = emp1[0]["company"];

        const ans = employees.map(obj => {

            if (obj.id === 93) {

                obj.company = a;
            }
            else if (obj.id === 92) {

                obj.company = b;
            }

            return obj;
        });

        fs.writeFile(path.join(outputPath, "swapedUsers.json"), JSON.stringify(ans, null, 4), (error) => {

            if (error) {

                reject(error);
                return;
            } else {

                resolve(data);
            }

        });

    });

}

// 7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

function addBirthday(data) {

    return new Promise ((resolve,reject)=>{

    const employees = data.employees;

    const ans = employees.map(obj => {

        if (obj.id % 2 === 0) {

            obj.birthday = new Date().toString().substring(4,15);
        }

        return obj;
    });


    fs.writeFile(path.join(outputPath, "evenBirthday.json"), JSON.stringify(ans, null, 4), (error) => {

        if (error) {

            reject(error);

        } else {

            console.log(("All operations executed sucessfully"));
            resolve(data);
        }

    });

});

}



module.exports.retriveid = retriveUsers;

module.exports.groupComponies = groupCompanies;

module.exports.retriveComp = retriveCompany;

module.exports.removeID = removeId;

module.exports.sortUser = sortUsers;

module.exports.swapcomp = swapCompany;

module.exports.evenBirthday = addBirthday;

